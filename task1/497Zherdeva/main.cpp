#include <Application.hpp>
#include <ShaderProgram.hpp>
#include <Mesh.hpp>
#include "Hyperboloid.hpp"



#include <iostream>
#include <vector>
#include <algorithm>



class SampleApplication : public Application {

private:
    const float MIN_TRIANGLES = 4.;
    const float MAX_TRIANGLES = 100.;
    float _n_triangles = 50.;
    float DT_MULTIPLIER = 200.;

    void updateNTriangles(double dt) {
        _n_triangles += dt * DT_MULTIPLIER;
        _n_triangles = std::max(MIN_TRIANGLES, _n_triangles);
        _n_triangles = std::min(MAX_TRIANGLES, _n_triangles);
    }

public:
    MeshPtr _hyperboloid;
    ShaderProgramPtr _shader;

    void makeScene() override {

        Application::makeScene();
        _cameraMover = std::make_shared<FreeCameraMover>();

        //Создаем меш из файла
        _hyperboloid = makeHyperboloid(static_cast<unsigned int>(_n_triangles));

        //Создаем шейдерную программу
        _shader = std::make_shared<ShaderProgram>("497ZherdevaData/shader.vert", "497ZherdevaData/shader.frag");
    }

    void update() override {
        double dt = glfwGetTime() - _oldTime;
        if (glfwGetKey(_window, GLFW_KEY_MINUS) == GLFW_PRESS) {
            updateNTriangles(-dt);
            _hyperboloid = makeHyperboloid(static_cast<unsigned int>(_n_triangles));
        }
        if (glfwGetKey(_window, GLFW_KEY_EQUAL) == GLFW_PRESS) {
            updateNTriangles(dt);
            _hyperboloid = makeHyperboloid(static_cast<unsigned int>(_n_triangles));
        }
        Application::update();
    }

  
    void draw() override {
        Application::draw();

        //Получаем текущие размеры экрана и выставлям вьюпорт

        int width, height;
        glfwGetFramebufferSize(_window, &width, &height);

        glViewport(0, 0, width, height);

        //Очищаем буферы цвета и глубины от результатов рендеринга предыдущего кадра

        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        //Подключаем шейдер
        _shader->use();

        //Устанавливаем общие юниформ-переменные

        _shader->setMat4Uniform("viewMatrix", _camera.viewMatrix);
        _shader->setMat4Uniform("projectionMatrix", _camera.projMatrix);

        //Загружаем на видеокарту матрицы модели мешей и запускаем отрисовку
        _shader->setMat4Uniform("modelMatrix", _hyperboloid->modelMatrix());

        //Рисуем фигуру
        _hyperboloid->draw();
    }

};

int main() {
    SampleApplication app;
    app.start();

    return 0;
}
