#include <iostream>
#include <vector>
#include <algorithm>

#include <Mesh.hpp>


glm::vec3 getParametrizedSurfacePoint( float s, float t ) {
    return glm::vec3( sqrt( s * s + 1 ) * cos( t ), sqrt( s * s + 1 ) * sin( t ), s );
}

glm::vec3 getParametrizedSurfaceNormal( float s, float t ) {
    return glm::normalize( 
        glm::vec3( 2 * sqrt( s * s + 1 ) * cos( t ), 2 * sqrt( s * s + 1 ) * sin( t ), -2 * s ) );
}

MeshPtr makeHyperboloid(unsigned int M) {


    std::vector<glm::vec3> vertices;
    std::vector<glm::vec3> normals;

    for (unsigned int i = 0; i < M; i++) {
        
        float s = -1 + static_cast<float>(2 * i) / M;
        float s1 = -1 + static_cast<float>(2 * (i + 1)) / M;

        for (unsigned int j = 0; j < M; j++) {
            float t = 2.0f * static_cast<float>(glm::pi<float>() * j / M);
            float t1 = 2.0f * static_cast<float>(glm::pi<float>() * (j + 1) / M);

            // First triangle
            vertices.push_back(getParametrizedSurfacePoint(s, t));
            vertices.push_back(getParametrizedSurfacePoint(s, t1));
            vertices.push_back(getParametrizedSurfacePoint(s1, t));

            normals.push_back(getParametrizedSurfaceNormal(s, t));
            normals.push_back(getParametrizedSurfaceNormal(s, t1));
            normals.push_back(getParametrizedSurfaceNormal(s1, t));

            //Second triangle
            vertices.push_back(getParametrizedSurfacePoint(s1, t1));
            vertices.push_back(getParametrizedSurfacePoint(s, t1));
            vertices.push_back(getParametrizedSurfacePoint(s1, t));

            normals.push_back(getParametrizedSurfaceNormal(s1, t1));
            normals.push_back(getParametrizedSurfaceNormal(s, t1));
            normals.push_back(getParametrizedSurfaceNormal(s1, t));
        }
    }

    //----------------------------------------

    DataBufferPtr buf0 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
    buf0->setData(vertices.size() * sizeof(float) * 3, vertices.data());

    DataBufferPtr buf1 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
    buf1->setData(normals.size() * sizeof(float) * 3, normals.data());

    MeshPtr mesh = std::make_shared<Mesh>();
    mesh->setAttribute(0, 3, GL_FLOAT, GL_FALSE, 0, 0, buf0);
    mesh->setAttribute(1, 3, GL_FLOAT, GL_FALSE, 0, 0, buf1);
    mesh->setPrimitiveType(GL_TRIANGLES);
    mesh->setVertexCount(vertices.size());

    std::cout << "Hyperboloid is created with " << vertices.size() << " vertices\n";

    return mesh;
}
